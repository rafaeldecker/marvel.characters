package com.marvel.characters.android.screens.base

import android.os.Bundle
import com.marvel.characters.util.extensions.observe

/**
 * Created by Rafael Decker on 2020-01-18.
 */

abstract class MvvmActivity<T : MvvmViewModel, R> : BaseActivity() {

    abstract val viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeViewModelState()
    }

    protected open fun observeViewModelState() {
        observe(viewModel.state, ::onViewModelStateChanged)
    }

    @Suppress("UNCHECKED_CAST")
    protected open fun onViewModelStateChanged(state: ViewModelState) {
        when(state) {
            is ViewModelState.Data<*> -> onSuccess(state.data as R)
            is ViewModelState.Loading -> onLoading()
            is ViewModelState.Error -> onError(state.throwable)
        }
    }

    abstract fun onSuccess(data: R)
    abstract fun onError(data: Throwable)
    abstract fun onLoading()


}