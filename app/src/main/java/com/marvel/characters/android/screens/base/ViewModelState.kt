package com.marvel.characters.android.screens.base

/**
 * Created by Rafael Decker on 2020-01-18.
 */

sealed class ViewModelState {

    data class Data<T>(val data: T) : ViewModelState()
    data class Error(val throwable: Throwable) : ViewModelState()
    object Loading : ViewModelState()

    fun isLoading() = this is Loading
    fun isData() = this is Data<*>
    fun isError() = this is Error

}

