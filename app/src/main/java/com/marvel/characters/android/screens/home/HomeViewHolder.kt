package com.marvel.characters.android.screens.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.R
import com.marvel.characters.util.ClickHandler
import com.marvel.characters.util.LongClickHandler
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.vh_home.view.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class HomeViewHolder(
    itemView: View,
    private val clickHandler: ClickHandler<HomeModel>? = null,
    private val longClickHandler: LongClickHandler<HomeModel>? = null
) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: HomeModel) {
        clickHandler?.let { handler ->
            itemView.cardView.setOnClickListener {
                handler.onClick(data)
            }
        }
        longClickHandler?.let { handler ->
            itemView.cardView.setOnLongClickListener {
                handler.onLongClick(data)
                true
            }
        }
        itemView.infoImageView.visibility = if (data.isInfoAvailable) {
            View.VISIBLE
        } else {
            View.GONE
        }
        itemView.titleTextView.text = data.name
        Picasso.get().load(data.thumbnail).into(itemView.thumbImageView)
    }

    companion object {

        fun newInstance(
            inflater: LayoutInflater,
            parent: ViewGroup?,
            clickHandler: ClickHandler<HomeModel>? = null,
            longClickHandler: LongClickHandler<HomeModel>? = null
        ): HomeViewHolder {
            val view = inflater.inflate(R.layout.vh_home, parent, false)
            return HomeViewHolder(view, clickHandler, longClickHandler)
        }

    }
}