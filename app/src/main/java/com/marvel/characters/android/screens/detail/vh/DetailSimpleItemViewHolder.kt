package com.marvel.characters.android.screens.detail.vh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.R
import com.marvel.characters.android.screens.detail.DetailSection
import kotlinx.android.synthetic.main.vh_detail_simple_text.view.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailSimpleItemViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: String) {
        itemView.titleTextView.text = data
    }

    companion object {

        fun newInstance(inflater: LayoutInflater, parent: ViewGroup?): DetailSimpleItemViewHolder {
            val view = inflater.inflate(R.layout.vh_detail_simple_text, parent, false)
            return DetailSimpleItemViewHolder(
                view
            )
        }

    }
}