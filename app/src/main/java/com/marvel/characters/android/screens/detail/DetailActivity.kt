package com.marvel.characters.android.screens.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.marvel.characters.R
import com.marvel.characters.android.screens.base.MvvmActivity
import com.marvel.characters.util.ClickHandler
import com.marvel.characters.util.extensions.openBrowser
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailActivity : MvvmActivity<DetailViewModel, DetailModel>(), ClickHandler<String> {

    override val viewModel: DetailViewModel by viewModel()

    private lateinit var adapter: DetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setupViews()

        intent.extras?.let {
            toolBar.title = it.getString(CHARACTER_NAME_KEY, getString(R.string.app_name))
            viewModel.load(it.getLong(CHARACTER_ID_KEY))
        }
    }

    private fun setupViews() {
        toolBar.setNavigationOnClickListener { finish() }
        val layoutManager = LinearLayoutManager(this)
        adapter = DetailAdapter(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    override fun onSuccess(data: DetailModel) {
        loadingProgressBar.visibility = View.GONE
        adapter.put(data.sections)
    }

    override fun onError(data: Throwable) {
        loadingProgressBar.visibility = View.GONE
        Toast.makeText(this, R.string.error_generic_message, Toast.LENGTH_LONG).show()
    }

    override fun onLoading() {
        loadingProgressBar.visibility = View.VISIBLE
    }

    override fun onClick(url: String) {
        openBrowser(url)
    }

    companion object {

        private const val CHARACTER_ID_KEY = "CHARACTER_ID_KEY"
        private const val CHARACTER_NAME_KEY = "CHARACTER_NAME_KEY"

        fun launch(context: Context, characterId: Long, characterName: String) {
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra(CHARACTER_ID_KEY, characterId)
            intent.putExtra(CHARACTER_NAME_KEY, characterName)
            context.startActivity(intent)
        }

    }

}