package com.marvel.characters.android.screens.home

/**
 * Created by Rafael Decker on 2020-01-18.
 */

data class HomeModel(
    val id: Long,
    val name: String,
    val description: String,
    val thumbnail: String,
    val isInfoAvailable: Boolean
)
