package com.marvel.characters.android.screens.home

import com.marvel.characters.android.screens.base.MvvmViewModel
import com.marvel.characters.android.screens.base.ViewModelState
import com.marvel.characters.domain.GetCharactersUseCase
import com.marvel.characters.entities.Character
import com.marvel.characters.infra.schedulers.RxSchedulerProvider
import com.marvel.characters.util.Mapper
import com.marvel.characters.util.extensions.isError
import com.marvel.characters.util.extensions.isLoading
import com.marvel.characters.util.extensions.saveMainThread

/**
 * Created by Rafael Decker on 2020-01-18.
 */

open class HomeViewModel(
    private val useCase: GetCharactersUseCase,
    private val mapper: Mapper<Character, HomeModel>,
    private val rxSchedulerProvider: RxSchedulerProvider
) : MvvmViewModel() {

    private var characters = ArrayList<HomeModel>()
    private var hasMoreData = true

    fun start() {
        if (canLoad() && characters.isEmpty()) {
            fetchNextData()
        }
    }

    fun loadMore() {
        if (canLoad() && hasMoreData) {
            fetchNextData()
        }
    }

    private fun canLoad() = !state.isLoading() || state.isError()

    private fun fetchNextData() {
        updateState(ViewModelState.Loading)
        addDisposable(
            useCase.fetch(characters.size.toLong(), NUMBER_OF_ITEMS_PER_REQUEST)
                .saveMainThread(rxSchedulerProvider)
                .map {
                    hasMoreData = it.size.toLong() == NUMBER_OF_ITEMS_PER_REQUEST
                    mapper.mapList(it)
                }
                .subscribe({
                    characters.addAll(it)
                    updateState(ViewModelState.Data(characters))
                }, {
                    updateState(ViewModelState.Error(it))
                })
        )
    }

    companion object {
        const val NUMBER_OF_ITEMS_PER_REQUEST = 20L
    }

}