package com.marvel.characters.android.screens.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.util.ClickHandler
import com.marvel.characters.util.LongClickHandler

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class HomeAdapter(
    private val clickHandler: ClickHandler<HomeModel>? = null,
    private val longClickHandler: LongClickHandler<HomeModel>? = null
): RecyclerView.Adapter<HomeViewHolder>() {

    private var dataSource: List<HomeModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return HomeViewHolder.newInstance(inflater, parent, clickHandler, longClickHandler)
    }

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val item = dataSource[position]
        holder.bind(item)
    }

    fun put(data: List<HomeModel>) {
        dataSource = data
        notifyDataSetChanged()
    }

}
