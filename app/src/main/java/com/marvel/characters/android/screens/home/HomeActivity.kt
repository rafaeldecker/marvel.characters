package com.marvel.characters.android.screens.home

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.GridLayoutManager
import com.marvel.characters.R
import com.marvel.characters.android.screens.base.MvvmActivity
import com.marvel.characters.android.screens.detail.DetailActivity
import com.marvel.characters.android.widgets.EqualSpacingItemDecoration
import com.marvel.characters.android.widgets.VerticalRecyclerViewScrollListener
import com.marvel.characters.util.ClickHandler
import com.marvel.characters.util.LongClickHandler
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import kotlin.math.floor

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class HomeActivity : MvvmActivity<HomeViewModel, List<HomeModel>>(),
    ClickHandler<HomeModel>,
    LongClickHandler<HomeModel> {

    override val viewModel: HomeViewModel by viewModel()

    private lateinit var adapter: HomeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupViews()
        viewModel.start()
    }

    private fun setupViews() {
        val cardWidth = resources.getDimensionPixelSize(R.dimen.character_card_width).toFloat()
        val screenWidth = resources.displayMetrics.widthPixels.toFloat()
        val remainingSpace = screenWidth % cardWidth
        val numberOfColumns = floor(screenWidth / cardWidth).toInt()
        val itemSpacing = (remainingSpace / (numberOfColumns + 2)).toInt()
        val layoutManager = GridLayoutManager(this, numberOfColumns)
        recyclerView.layoutManager = layoutManager
        adapter = HomeAdapter(this, this)
        recyclerView.addOnScrollListener(VerticalRecyclerViewScrollListener {
            viewModel.loadMore()
        })
        recyclerView.addItemDecoration(EqualSpacingItemDecoration(itemSpacing))
        recyclerView.adapter = adapter
    }

    override fun onSuccess(data: List<HomeModel>) {
        updateLoading(false)
        adapter.put(data)
    }

    override fun onLoading() {
        updateLoading(true)
    }

    override fun onError(data: Throwable) {
        updateLoading(false)
        AlertDialog.Builder(this)
            .setTitle(R.string.loading_data_fail_retry_title)
            .setMessage(R.string.loading_data_fail_retry_message)
            .setPositiveButton(R.string.yes) { dialog, _ ->
                dialog.dismiss()
                viewModel.loadMore()
            }
            .setNegativeButton(R.string.no, null)
            .show()
    }

    override fun onClick(item: HomeModel) {
        DetailActivity.launch(this, item.id, item.name)
    }

    override fun onLongClick(item: HomeModel) {
        if (item.isInfoAvailable) {
            showInfo(item)
        }
    }

    private fun updateLoading(isLoading: Boolean) {
        val visibility = if (isLoading) {
            View.VISIBLE
        } else {
            View.GONE
        }
        loadingProgressBar.visibility = visibility
        loadingLayout.visibility = visibility
    }

    private fun showInfo(character: HomeModel) {
        AlertDialog.Builder(this)
            .setTitle(character.name)
            .setMessage(character.description)
            .setNegativeButton(R.string.ok, null)
            .show()
    }

}
