package com.marvel.characters.android.widgets

import androidx.recyclerview.widget.RecyclerView


class VerticalRecyclerViewScrollListener(
    private val onReachedBottom: () -> Unit
) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (!recyclerView.canScrollVertically(-1)) {
            onScrolledToTop()
        } else if (!recyclerView.canScrollVertically(1)) {
            onScrolledToBottom()
        } else if (dy < 0) {
            onScrolledUp()
        } else if (dy > 0) {
            onScrolledDown()
        }
    }

    private fun onScrolledUp() {}
    private fun onScrolledDown() {}
    private fun onScrolledToTop() {}
    private fun onScrolledToBottom() {
        onReachedBottom.invoke()
    }
}