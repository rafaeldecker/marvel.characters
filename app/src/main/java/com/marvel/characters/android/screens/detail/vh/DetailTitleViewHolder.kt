package com.marvel.characters.android.screens.detail.vh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.R
import kotlinx.android.synthetic.main.vh_detail_title.view.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailTitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: String) {
        itemView.titleTextView.text = data
    }

    companion object {

        fun newInstance(
            inflater: LayoutInflater,
            parent: ViewGroup?
        ): DetailTitleViewHolder {
            val view = inflater.inflate(R.layout.vh_detail_title, parent, false)
            return DetailTitleViewHolder(
                view
            )
        }

    }
}