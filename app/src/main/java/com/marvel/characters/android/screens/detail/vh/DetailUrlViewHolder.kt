package com.marvel.characters.android.screens.detail.vh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.R
import com.marvel.characters.android.screens.detail.DetailSection
import com.marvel.characters.util.ClickHandler
import kotlinx.android.synthetic.main.vh_detail_url.view.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailUrlViewHolder(
    itemView: View,
    private val clickHandler: ClickHandler<String>? = null
) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: DetailSection.UrlDetail) {
        itemView.urlTypeTextView.text = data.type.toUpperCase()
        itemView.urlTextView.text = data.url
        itemView.setOnClickListener {
            clickHandler?.onClick(data.url)
        }
    }

    companion object {

        fun newInstance(
            inflater: LayoutInflater,
            parent: ViewGroup?,
            clickHandler: ClickHandler<String>? = null
        ): DetailUrlViewHolder {
            val view = inflater.inflate(R.layout.vh_detail_url, parent, false)
            return DetailUrlViewHolder(
                view,
                clickHandler
            )
        }

    }
}