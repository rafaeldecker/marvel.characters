package com.marvel.characters.android.screens.detail

import com.marvel.characters.domain.ThumbnailUrlBuilder
import com.marvel.characters.entities.Character
import com.marvel.characters.util.Mapper
import com.marvel.characters.util.extensions.toMmDdYyyy

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailModelMapper(
    private val thumbnailUrlBuilder: ThumbnailUrlBuilder
) : Mapper<Character, DetailModel> {

    override fun map(param: Character): DetailModel {
        val sections = mutableListOf<DetailSection>()
        detail(param)?.let {
            sections.add(it)
        }
        comics(param)?.let {
            sections.add(it)
            sections.addAll(it.list.map { str -> DetailSection.SimpleItem(str) })
        }
        series(param)?.let {
            sections.add(it)
            sections.addAll(it.list.map { str -> DetailSection.SimpleItem(str) })
        }
        stories(param)?.let {
            sections.add(it)
            sections.addAll(it.list.map { str -> DetailSection.SimpleItem(str) })
        }
        urls(param)?.let {
            sections.add(it)
            sections.addAll(it.list)
        }
        return DetailModel(sections)
    }

    override fun mapReverse(param: DetailModel): Character {
        throw NotImplementedError("DetailModelMapper::mapReverse not implemented")
    }

    private fun detail(character: Character): DetailSection.Character? {
        with(character) {
            val desc = if (description.isNotEmpty()) {
                description
            } else {
                null
            }
            return DetailSection.Character(
                id,
                name,
                desc,
                thumbnailUrlBuilder.build(thumbnail),
                modified.toMmDdYyyy()
            )
        }
    }

    private fun comics(character: Character): DetailSection.Comics? {
        if (character.comics.isNotEmpty()) {
            return DetailSection.Comics(character.comics)
        }
        return null
    }

    private fun series(character: Character): DetailSection.Series? {
        if (character.series.isNotEmpty()) {
            return DetailSection.Series(character.series)
        }
        return null
    }

    private fun stories(character: Character): DetailSection.Stories? {
        if (character.stories.isNotEmpty()) {
            return DetailSection.Stories(character.stories)
        }
        return null
    }

    private fun urls(character: Character): DetailSection.Urls? {
        if (character.urls.isNotEmpty()) {
            return DetailSection.Urls(character.urls.map {
                DetailSection.UrlDetail(
                    it.type,
                    it.url
                )
            })
        }
        return null
    }

}