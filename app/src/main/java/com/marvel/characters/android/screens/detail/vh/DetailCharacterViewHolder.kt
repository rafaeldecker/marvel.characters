package com.marvel.characters.android.screens.detail.vh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.R
import com.marvel.characters.android.screens.detail.DetailSection
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.vh_detail_character.view.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailCharacterViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(data: DetailSection.Character) {
        itemView.titleTextView.text = data.name
        Picasso.get().load(data.thumbnail).into(itemView.thumbImageView)
        itemView.modifiedTextView.text = data.modified
        itemView.descriptionTextView.text = data.description
    }

    companion object {

        fun newInstance(inflater: LayoutInflater, parent: ViewGroup?): DetailCharacterViewHolder {
            val view = inflater.inflate(R.layout.vh_detail_character, parent, false)
            return DetailCharacterViewHolder(
                view
            )
        }

    }
}