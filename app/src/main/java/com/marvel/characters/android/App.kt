package com.marvel.characters.android

import android.app.Application
import com.marvel.characters.infra.logger.Logger
import com.marvel.characters.injection.AppInjector
import org.koin.android.ext.android.inject

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class App : Application() {

    private val logger: Logger by inject()

    override fun onCreate() {
        super.onCreate()
        startInjection()
        startLogger()
    }

    private fun startInjection() {
        AppInjector.inject(this)
    }

    private fun startLogger() {
        logger.start()
    }

}
