package com.marvel.characters.android.screens.detail

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailModel(
    val sections: List<DetailSection>
)

sealed class DetailSection {
    data class Character(
        val id: Long,
        val name: String,
        val description: String? = null,
        val thumbnail: String,
        val modified: String
    ) : DetailSection()

    data class Comics(val list: List<String>) : DetailSection()
    data class Series(val list: List<String>) : DetailSection()
    data class Stories(val list: List<String>) : DetailSection()
    data class Urls(val list: List<UrlDetail>) : DetailSection()
    data class UrlDetail(val type: String, val url: String) : DetailSection()
    data class SimpleItem(val data: String) : DetailSection()

}



