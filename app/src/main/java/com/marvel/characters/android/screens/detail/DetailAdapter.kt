package com.marvel.characters.android.screens.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.characters.R
import com.marvel.characters.android.screens.detail.vh.DetailCharacterViewHolder
import com.marvel.characters.android.screens.detail.vh.DetailSimpleItemViewHolder
import com.marvel.characters.android.screens.detail.vh.DetailTitleViewHolder
import com.marvel.characters.android.screens.detail.vh.DetailUrlViewHolder
import com.marvel.characters.util.ClickHandler

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailAdapter(
    private val clickHandler: ClickHandler<String>? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var dataSource: List<DetailSection> = emptyList()

    override fun getItemViewType(position: Int): Int {
        return when (dataSource[position]) {
            is DetailSection.Character -> DETAIL_VIEW_TYPE
            is DetailSection.Comics -> TITLE_VIEW_TYPE
            is DetailSection.Stories -> TITLE_VIEW_TYPE
            is DetailSection.Series -> TITLE_VIEW_TYPE
            is DetailSection.Urls -> TITLE_VIEW_TYPE
            is DetailSection.UrlDetail -> URL_VIEW_TYPE
            is DetailSection.SimpleItem -> SIMPLE_ITEM_VIEW_TYPE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            DETAIL_VIEW_TYPE -> DetailCharacterViewHolder.newInstance(inflater, parent)
            TITLE_VIEW_TYPE -> DetailTitleViewHolder.newInstance(inflater, parent)
            URL_VIEW_TYPE -> DetailUrlViewHolder.newInstance(inflater, parent, clickHandler)
            SIMPLE_ITEM_VIEW_TYPE -> DetailSimpleItemViewHolder.newInstance(inflater, parent)
            else -> throw Throwable("Could not instantiate vh")
        }
    }

    override fun getItemCount(): Int = dataSource.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = dataSource[position]) {
            is DetailSection.Character -> (holder as DetailCharacterViewHolder).bind(item)
            is DetailSection.Comics -> {
                val title = getString(holder, R.string.comics)
                (holder as DetailTitleViewHolder).bind(title)
            }
            is DetailSection.Stories -> {
                val title = getString(holder, R.string.stories)
                (holder as DetailTitleViewHolder).bind(title)
            }
            is DetailSection.Series -> {
                val title = getString(holder, R.string.series)
                (holder as DetailTitleViewHolder).bind(title)
            }
            is DetailSection.Urls -> {
                val title = getString(holder, R.string.links)
                (holder as DetailTitleViewHolder).bind(title)
            }
            is DetailSection.UrlDetail -> {
                (holder as DetailUrlViewHolder).bind(item)
            }
            is DetailSection.SimpleItem -> {
                (holder as DetailSimpleItemViewHolder).bind(item.data)
            }
        }
    }

    private fun getString(holder: RecyclerView.ViewHolder, resId: Int): String {
        return holder.itemView.context.getString(resId)
    }

    fun put(data: List<DetailSection>) {
        dataSource = data
        notifyDataSetChanged()
    }

    companion object {
        private const val DETAIL_VIEW_TYPE = 1
        private const val TITLE_VIEW_TYPE = 2
        private const val SIMPLE_ITEM_VIEW_TYPE = 3
        private const val URL_VIEW_TYPE = 4
    }

}