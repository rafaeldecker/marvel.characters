package com.marvel.characters.android.screens.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marvel.characters.util.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Rafael Decker on 2020-01-18.
 */

abstract class MvvmViewModel: ViewModel() {

    private val disposables = CompositeDisposable()

    private val mutableState = MutableLiveData<ViewModelState>()

    val state: LiveData<ViewModelState> by lazy { mutableState }

    protected fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    override fun onCleared() {
        disposables.dispose()
        super.onCleared()
    }

    protected open fun updateState(state: ViewModelState) = mutableState.postValue(state)

}