package com.marvel.characters.android.screens.detail

import com.marvel.characters.android.screens.base.MvvmViewModel
import com.marvel.characters.android.screens.base.ViewModelState
import com.marvel.characters.android.screens.home.HomeModel
import com.marvel.characters.domain.GetCharactersUseCase
import com.marvel.characters.entities.Character
import com.marvel.characters.infra.schedulers.RxSchedulerProvider
import com.marvel.characters.util.Mapper
import com.marvel.characters.util.extensions.saveMainThread

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailViewModel(
    private val useCase: GetCharactersUseCase,
    private val modelMapper: Mapper<Character, DetailModel>,
    private val rxSchedulerProvider: RxSchedulerProvider
) : MvvmViewModel() {

    fun load(characterId: Long) {
        updateState(ViewModelState.Loading)
        addDisposable(
            useCase.fetchDetail(characterId)
                .map { modelMapper.map(it) }
                .saveMainThread(rxSchedulerProvider)
                .subscribe({
                    updateState(ViewModelState.Data(it))
                },{
                    updateState(ViewModelState.Error(it))
                })
        )
    }

}