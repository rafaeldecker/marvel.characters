package com.marvel.characters.android.screens.home

import com.marvel.characters.domain.ThumbnailUrlBuilder
import com.marvel.characters.entities.Character
import com.marvel.characters.util.Mapper

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class HomeModelMapper(
    private val thumbnailUrlBuilder: ThumbnailUrlBuilder
) : Mapper<Character, HomeModel> {

    override fun map(param: Character): HomeModel =
        with(param) {
            HomeModel(
                id = id,
                name = name,
                description = description,
                thumbnail = thumbnailUrlBuilder.build(thumbnail),
                isInfoAvailable = description.isNotEmpty()
            )
        }

    override fun mapReverse(param: HomeModel): Character {
        throw NotImplementedError("HomeModelMapper::mapReverse not implemented")
    }

}