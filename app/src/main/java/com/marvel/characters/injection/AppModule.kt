package com.marvel.characters.injection

import com.marvel.characters.infra.logger.Logger
import com.marvel.characters.infra.logger.TimberLogger
import com.marvel.characters.infra.schedulers.RxSchedulerProvider
import com.marvel.characters.infra.schedulers.RxSchedulerProviderImpl
import com.marvel.characters.infra.timestamp.TimestampProvider
import com.marvel.characters.infra.timestamp.TimestampProviderImpl
import org.koin.core.module.Module
import org.koin.dsl.module

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class AppModule : ComponentModule {

    override fun provides(): Module =
        module {
            factory<Logger> { TimberLogger() }
            factory<RxSchedulerProvider> { RxSchedulerProviderImpl() }
            factory<TimestampProvider> { TimestampProviderImpl() }
        }

}