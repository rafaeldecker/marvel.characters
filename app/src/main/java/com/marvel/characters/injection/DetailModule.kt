package com.marvel.characters.injection

import com.marvel.characters.android.screens.detail.DetailModelMapper
import com.marvel.characters.android.screens.detail.DetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailModule : ComponentModule {

    override fun provides(): Module =
        module {
            factory(named(DETAIL_MODEL_MAPPER)) { DetailModelMapper(get()) }
            viewModel { DetailViewModel(get(), get(named(DETAIL_MODEL_MAPPER)), get()) }
        }

    companion object {
        private const val DETAIL_MODEL_MAPPER = "DETAIL_MODEL_MAPPER"
    }

}