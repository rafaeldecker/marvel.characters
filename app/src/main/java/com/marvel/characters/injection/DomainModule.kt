package com.marvel.characters.injection

import com.marvel.characters.domain.*
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class DomainModule : ComponentModule {

    override fun provides(): Module =
        module {
            factory<ThumbnailUrlBuilder> { ThumbnailUrlBuildImpl() }
            factory(named(CHARACTER_MAPPER)) { CharacterMapper() }
            factory<GetCharactersUseCase> { GetCharactersUseCaseImpl(get(), get(), get(named(CHARACTER_MAPPER))) }
        }

    companion object {
        private const val CHARACTER_MAPPER = "CHARACTER_MAPPER"
    }

}