package com.marvel.characters.injection

import org.koin.core.module.Module

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface ComponentModule {

    fun provides(): Module
}
