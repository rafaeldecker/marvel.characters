package com.marvel.characters.injection

import com.marvel.characters.infra.api.CharactersApi
import com.marvel.characters.infra.api.RetrofitFactory
import com.marvel.characters.infra.api.authorization.ApiAuthorizationProvider
import com.marvel.characters.infra.api.authorization.ApiAuthorizationProviderImpl
import com.marvel.characters.infra.api.keys.ApiKeysProvider
import com.marvel.characters.infra.api.keys.ApiKeysProviderImpl
import com.marvel.characters.infra.logger.Logger
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ApiModule : ComponentModule {

    override fun provides(): Module =
        module {
            single<OkHttpClient> {
                val logger: Logger = get()
                val client = OkHttpClient.Builder()
                val httpLogger = HttpLoggingInterceptor.Logger { logger.d("OkHttp", it) }
                val logging = HttpLoggingInterceptor(httpLogger)
                logging.level = HttpLoggingInterceptor.Level.BODY
                client.interceptors().add(logging)
                client.build()
            }
            single { RetrofitFactory(get()).create() }
            single { get<Retrofit>().create(CharactersApi::class.java) }

            factory<ApiKeysProvider> { ApiKeysProviderImpl() }
            factory<ApiAuthorizationProvider> { ApiAuthorizationProviderImpl(get(), get()) }
        }

}