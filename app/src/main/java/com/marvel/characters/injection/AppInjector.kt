package com.marvel.characters.injection

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Rafael Decker on 2020-01-18.
 */

object AppInjector {

    fun inject(application: Application) {
        startKoin {
            androidContext(application)
            modules(
                listOf(
                    AppModule().provides(),
                    ApiModule().provides(),
                    DomainModule().provides(),

                    HomeModule().provides(),
                    DetailModule().provides()
                )
            )
        }
    }

}
