package com.marvel.characters.injection

import com.marvel.characters.android.screens.home.HomeModel
import com.marvel.characters.android.screens.home.HomeModelMapper
import com.marvel.characters.android.screens.home.HomeViewModel
import com.marvel.characters.entities.Character
import com.marvel.characters.util.Mapper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class HomeModule : ComponentModule {

    override fun provides(): Module =
        module {
            factory(named(HOME_MODEL_MAPPER)) { HomeModelMapper(get()) }
            viewModel { HomeViewModel(get(), get(named(HOME_MODEL_MAPPER)), get()) }
        }

    companion object {
        private const val HOME_MODEL_MAPPER = "HOME_MODEL_MAPPER"
    }
}