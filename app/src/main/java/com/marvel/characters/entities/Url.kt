package com.marvel.characters.entities

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class Url(
    val type: String,
    val url: String
)
