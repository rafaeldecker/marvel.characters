package com.marvel.characters.entities

import java.util.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

data class Character(
    val id: Long,
    val name: String,
    val description: String,
    val thumbnail: Thumbnail,
    val modified: Date,
    val comics: List<String>,
    val series: List<String>,
    val stories: List<String>,
    val urls: List<Url>
)
