package com.marvel.characters.entities

/**
 * Created by Rafael Decker on 2020-01-18.
 */

data class Thumbnail(
    val path: String,
    val extension: String
)
