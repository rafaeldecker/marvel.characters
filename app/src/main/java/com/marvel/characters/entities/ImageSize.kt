package com.marvel.characters.entities

/**
 * Created by Rafael Decker on 2020-01-18.
 */

enum class ImageSize(val rawValue: String) {

    PORTRAIT_SMALL("portrait_small"), // 50x75px
    PORTRAIT_MEDIUM("portrait_medium"), // 100x150px
    PORTRAIT_XLARGE("portrait_xlarge"), // 150x225px
    PORTRAIT_FANTASTIC("portrait_fantastic"), // 168x252px
    PORTRAIT_UNCANNY("portrait_uncanny"),    // 300x450px
    PORTRAIT_INCREDIBLE("portrait_incredible"), // 216x324px

}
