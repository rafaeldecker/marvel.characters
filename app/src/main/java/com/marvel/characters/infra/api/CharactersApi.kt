package com.marvel.characters.infra.api

import com.marvel.characters.infra.api.models.ApiResponse
import com.marvel.characters.infra.api.models.CharacterResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface CharactersApi {

    @GET("/v1/public/characters")
    fun get(
        @Query("ts") timeStamp: Long,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String,
        @Query("limit") limit: Long,
        @Query("offset") offset: Long
    ): Observable<ApiResponse<CharacterResponse>>

    @GET("/v1/public/characters/{characterId}")
    fun getCharacter(
        @Path("characterId") characterId: Long,
        @Query("ts") timeStamp: Long,
        @Query("apikey") apiKey: String,
        @Query("hash") hash: String
    ): Single<ApiResponse<CharacterResponse>>

}