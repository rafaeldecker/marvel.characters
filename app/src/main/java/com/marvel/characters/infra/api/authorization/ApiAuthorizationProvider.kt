package com.marvel.characters.infra.api.authorization

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface ApiAuthorizationProvider {

    fun generate(): ApiAuthorization

}
