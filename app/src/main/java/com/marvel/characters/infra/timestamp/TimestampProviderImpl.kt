package com.marvel.characters.infra.timestamp

import java.util.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class TimestampProviderImpl : TimestampProvider {

    override fun time(): Long = Date().time

}