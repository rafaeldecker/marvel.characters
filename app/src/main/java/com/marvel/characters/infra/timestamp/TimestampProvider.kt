package com.marvel.characters.infra.timestamp

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface TimestampProvider {

    fun time(): Long

}