package com.marvel.characters.infra.api.authorization

/**
 * Created by Rafael Decker on 2020-01-18.
 */

data class ApiAuthorization(
    val timestamp: Long,
    val apiKey: String,
    val hash: String
)
