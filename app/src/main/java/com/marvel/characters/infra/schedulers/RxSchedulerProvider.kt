package com.marvel.characters.infra.schedulers

import io.reactivex.Scheduler

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface RxSchedulerProvider {

    fun io(): Scheduler

    fun computation(): Scheduler

    fun main(): Scheduler

}
