package com.marvel.characters.infra.api.models

/**
 * Created by Rafael Decker on 2020-01-18.
 */

open class ItemResponse(
    val resourceURI: String,
    val name: String
)
