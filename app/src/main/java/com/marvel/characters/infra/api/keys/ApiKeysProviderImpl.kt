package com.marvel.characters.infra.api.keys

import com.marvel.characters.BuildConfig

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ApiKeysProviderImpl : ApiKeysProvider {

    override fun getKeys(): ApiKeys = ApiKeys(
        public = BuildConfig.API_PUBLIC_KEY,
        private = BuildConfig.API_PRIVATE_KEY
    )

}
