package com.marvel.characters.infra.api.models

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class CollectionResponse<out T>(
    val collectionURI: String,
    val items: List<T>
)
