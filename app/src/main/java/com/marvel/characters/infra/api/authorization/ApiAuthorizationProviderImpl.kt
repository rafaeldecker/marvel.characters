package com.marvel.characters.infra.api.authorization

import com.marvel.characters.infra.api.keys.ApiKeys
import com.marvel.characters.infra.api.keys.ApiKeysProvider
import com.marvel.characters.infra.timestamp.TimestampProvider
import com.marvel.characters.util.extensions.toMd5

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ApiAuthorizationProviderImpl(
    private val apiKeysProvider: ApiKeysProvider,
    private val timestampProvider: TimestampProvider
) : ApiAuthorizationProvider {

    override fun generate(): ApiAuthorization {
        val timestamp = timestampProvider.time()
        val keys = apiKeysProvider.getKeys()
        return ApiAuthorization(
            timestamp = timestamp,
            apiKey = keys.public,
            hash = buildHash(timestamp, keys)
        )
    }

    private fun buildHash(timeStamp: Long, keys: ApiKeys): String {
        val input = "$timeStamp${keys.private}${keys.public}"
        return input.toMd5()
    }

}
