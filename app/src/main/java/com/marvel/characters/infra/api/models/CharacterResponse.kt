package com.marvel.characters.infra.api.models

import java.util.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class CharacterResponse(
    val id: Long,
    val name: String,
    val description: String,
    val modified: Date,
    val thumbnail: ThumbnailResponse,
    val resourceURI: String,
    val comics: CollectionResponse<ItemResponse>,
    val series: CollectionResponse<ItemResponse>,
    val stories: CollectionResponse<TypedItem>,
    val urls: List<UrlResponse>
)
