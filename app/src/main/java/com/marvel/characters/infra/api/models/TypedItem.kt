package com.marvel.characters.infra.api.models

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class TypedItem(
    resourceURI: String,
    name: String,
    val type: String
) : ItemResponse(resourceURI, name)
