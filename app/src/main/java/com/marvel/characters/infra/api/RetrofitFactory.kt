package com.marvel.characters.infra.api

import com.google.gson.GsonBuilder
import com.marvel.characters.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by Rafael Decker on 2020-01-18.
 */

class RetrofitFactory(
    private val okHttpClient: OkHttpClient
) {

    fun create(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

}
