package com.marvel.characters.infra.logger

import com.marvel.characters.BuildConfig
import timber.log.Timber

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class TimberLogger : Logger {

    override fun start() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun d(tag: String, message: String?) {
        Timber.tag(tag).d(message)
    }

    override fun d(message: String?) {
        Timber.d(message)
    }

    override fun d(throwable: Throwable) {
        Timber.d(throwable.toString())
    }

    override fun e(tag: String, message: String?) {
        Timber.tag(tag).e(message)
    }

    override fun e(message: String?) {
        Timber.e(message)
    }

    override fun e(throwable: Throwable) {
        Timber.e(throwable.toString())
    }

}