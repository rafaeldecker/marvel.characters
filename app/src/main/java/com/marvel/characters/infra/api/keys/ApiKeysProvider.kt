package com.marvel.characters.infra.api.keys

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface ApiKeysProvider {

    fun getKeys(): ApiKeys

}
