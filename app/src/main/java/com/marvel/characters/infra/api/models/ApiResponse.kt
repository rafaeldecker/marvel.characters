package com.marvel.characters.infra.api.models

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ApiResponse<out T>(
    val data: PaginationResponse<T>
)
