package com.marvel.characters.infra.api.keys

/**
 * Created by Rafael Decker on 2020-01-18.
 */

data class ApiKeys(
    val public: String,
    val private: String
)