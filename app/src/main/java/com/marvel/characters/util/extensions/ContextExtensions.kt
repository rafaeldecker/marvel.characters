package com.marvel.characters.util.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri

/**
 * Created by Rafael Decker on 2020-01-19.
 */

fun Context.openBrowser(url: String) {
    val finalUrl = if (!url.startsWith("http://") && !url.startsWith("https://")) {
        "http://$url"
    } else {
        url
    }
    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(finalUrl))
    startActivity(browserIntent)
}
