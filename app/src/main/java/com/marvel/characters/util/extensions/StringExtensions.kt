package com.marvel.characters.util.extensions

import java.math.BigInteger
import java.security.MessageDigest

/**
 * Created by Rafael Decker on 2020-01-18.
 */

fun String.toMd5(): String {
    val md = MessageDigest.getInstance("MD5")
    val bt = md.digest(toByteArray())
    return BigInteger(1, bt).toString(16)
        .padStart(32, '0')
}