package com.marvel.characters.util

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface LongClickHandler<T> {

    fun onLongClick(item: T)

}