package com.marvel.characters.util.extensions

import androidx.lifecycle.LiveData
import com.marvel.characters.android.screens.base.ViewModelState

/**
 * Created by Rafael Decker on 2020-01-18.
 */

fun LiveData<ViewModelState>.isLoading() = this.value?.isLoading() ?: false

fun LiveData<ViewModelState>.isData() = this.value?.isData() ?: false

fun LiveData<ViewModelState>.isError() = this.value?.isError() ?: false
