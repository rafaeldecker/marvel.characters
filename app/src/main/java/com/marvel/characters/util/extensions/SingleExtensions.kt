package com.marvel.characters.util.extensions

import com.marvel.characters.infra.schedulers.RxSchedulerProvider
import io.reactivex.Single

/**
 * Created by Rafael Decker on 2020-01-18.
 */

fun <R> Single<R>.subscribeOnIo(
    rxSchedulerProvider: RxSchedulerProvider
): Single<R> = subscribeOn(rxSchedulerProvider.io())

fun <R> Single<R>.observeOnMainThread(
    rxSchedulerProvider: RxSchedulerProvider
): Single<R> = observeOn(rxSchedulerProvider.main())

fun <R> Single<R>.observeOnIo(
    rxSchedulerProvider: RxSchedulerProvider
): Single<R> = observeOn(rxSchedulerProvider.io())

fun <R> Single<R>.saveMainThread(
    rxSchedulerProvider: RxSchedulerProvider
): Single<R> = subscribeOnIo(rxSchedulerProvider)
    .observeOnMainThread(rxSchedulerProvider)
