package com.marvel.characters.util.extensions

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

@SuppressLint("SimpleDateFormat")
fun Date.toString(format: String): String = SimpleDateFormat(format).format(this)

fun Date.toMmDdYyyy(): String = toString("MM/dd/yyyy")