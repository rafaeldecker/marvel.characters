package com.marvel.characters.domain

import com.marvel.characters.entities.ImageSize
import com.marvel.characters.entities.Thumbnail

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ThumbnailUrlBuildImpl : ThumbnailUrlBuilder {

    override fun build(thumb: Thumbnail, size: ImageSize): String =
        "${thumb.path}/${size.rawValue}.${thumb.extension}"

}