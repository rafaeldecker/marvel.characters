package com.marvel.characters.domain

import com.marvel.characters.entities.Character
import com.marvel.characters.infra.api.CharactersApi
import com.marvel.characters.infra.api.authorization.ApiAuthorizationProvider
import com.marvel.characters.infra.api.models.CharacterResponse
import com.marvel.characters.util.Mapper
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class GetCharactersUseCaseImpl(
    private val apiAuthorizationProvider: ApiAuthorizationProvider,
    private val api: CharactersApi,
    private val mapper: Mapper<CharacterResponse, Character>
) : GetCharactersUseCase {

    override fun fetch(offset: Long, limit: Long): Observable<List<Character>> {
        val auth = apiAuthorizationProvider.generate()
        return api.get(
            timeStamp = auth.timestamp,
            apiKey = auth.apiKey,
            hash = auth.hash,
            offset = offset,
            limit = limit
        ).map {
            mapper.mapList(it.data.results)
        }
    }

    override fun fetchDetail(id: Long): Single<Character> {
        val auth = apiAuthorizationProvider.generate()
        return api.getCharacter(
            characterId = id,
            timeStamp = auth.timestamp,
            apiKey = auth.apiKey,
            hash = auth.hash
        ).map {
            val mapped = mapper.mapList(it.data.results)
            if (mapped.isNotEmpty()) {
                mapped.first()
            } else {
                throw Throwable("Couldn't load content")
            }
        }
    }

}