package com.marvel.characters.domain

import com.marvel.characters.entities.ImageSize
import com.marvel.characters.entities.Thumbnail

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface ThumbnailUrlBuilder {

    fun build(thumb: Thumbnail, size: ImageSize = ImageSize.PORTRAIT_XLARGE): String

}