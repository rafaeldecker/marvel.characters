package com.marvel.characters.domain

import com.marvel.characters.entities.Character
import com.marvel.characters.entities.Thumbnail
import com.marvel.characters.entities.Url
import com.marvel.characters.infra.api.models.CharacterResponse
import com.marvel.characters.util.Mapper

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class CharacterMapper : Mapper<CharacterResponse, Character> {

    override fun map(param: CharacterResponse): Character =
        with(param) {
            Character(
                id = id,
                name = name,
                description = description,
                thumbnail = Thumbnail(
                    thumbnail.path,
                    thumbnail.extension
                ),
                modified = modified,
                comics = comics.items.map { it.name },
                series = series.items.map { it.name },
                stories = stories.items.map { it.name },
                urls = urls.map { Url(it.type, it.url) }
            )
        }

    override fun mapReverse(param: Character): CharacterResponse {
        throw NotImplementedError("CharacterMapper::mapReverse is not implemented")
    }

}