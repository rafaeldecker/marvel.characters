package com.marvel.characters.domain

import com.marvel.characters.entities.Character
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Created by Rafael Decker on 2020-01-18.
 */

interface GetCharactersUseCase {

    fun fetch(offset: Long, limit: Long): Observable<List<Character>>
    fun fetchDetail(id: Long): Single<Character>

}
