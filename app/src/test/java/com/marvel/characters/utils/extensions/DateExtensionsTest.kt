package com.marvel.characters.utils.extensions

import com.marvel.characters.util.extensions.toMmDdYyyy
import com.marvel.characters.util.extensions.toString
import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DateExtensionsTest {

    @Test
    fun `test MM-dd-yyyy date format`() {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, 2020)
        calendar.set(Calendar.MONTH, 11)
        calendar.set(Calendar.DAY_OF_MONTH, 16)
        assertEquals("12/16/2020", calendar.time.toMmDdYyyy())
    }

    @Test
    fun `test dd-MM-yyyy date format`() {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, 2020)
        calendar.set(Calendar.MONTH, 8)
        calendar.set(Calendar.DAY_OF_MONTH, 16)
        assertEquals("16/09/2020", calendar.time.toString("dd/MM/yyyy"))
    }
}