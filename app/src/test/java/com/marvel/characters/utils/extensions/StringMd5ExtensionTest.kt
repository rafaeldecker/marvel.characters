package com.marvel.characters.utils.extensions

import com.marvel.characters.util.extensions.toMd5
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class StringMd5ExtensionTest {

    @Test
    fun `test string to md5 conversion`() {
        val plainText = "test"
        assertEquals("098f6bcd4621d373cade4e832627b4f6", plainText.toMd5())
    }

}