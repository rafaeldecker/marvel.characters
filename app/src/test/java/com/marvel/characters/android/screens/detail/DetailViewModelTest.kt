package com.marvel.characters.android.screens.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.marvel.characters.android.screens.CharacterMock
import com.marvel.characters.android.screens.base.ViewModelState
import com.marvel.characters.domain.GetCharactersUseCase
import com.marvel.characters.domain.ThumbnailUrlBuildImpl
import com.marvel.characters.entities.Character
import com.marvel.characters.infra.schedulers.TrampolineRxSchedulerProvider
import com.marvel.characters.util.Mapper
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.internal.verification.Times

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var useCase: GetCharactersUseCase

    @Mock
    lateinit var mapper: Mapper<Character, DetailModel>

    private lateinit var viewModel: DetailViewModel


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = DetailViewModel(useCase, mapper, TrampolineRxSchedulerProvider())
    }

    @Test
    fun `test given use case response is calling model mapper`() {
        val character = CharacterMock.getItems(1).first()
        `when`(useCase.fetchDetail(character.id)).thenReturn(Single.just(character))
        viewModel.load(character.id)
        verify(mapper, Times(1)).map(character)
    }

    @Test
    fun `test given use case response is calling model mapper and updating state to data`() {
        val character = CharacterMock.getItems(1).first()
        `when`(useCase.fetchDetail(character.id)).thenReturn(Single.just(character))
        viewModel = DetailViewModel(useCase, DetailModelMapper(ThumbnailUrlBuildImpl()),TrampolineRxSchedulerProvider())
        viewModel.load(character.id)
        Assert.assertTrue(viewModel.state.value is ViewModelState.Data<*>)
    }

    @Test
    fun `test given use case response error is updating state to error`() {
        val character = CharacterMock.getItems(1).first()
        `when`(useCase.fetchDetail(character.id)).thenReturn(Single.error(Throwable("error")))
        viewModel = DetailViewModel(useCase, DetailModelMapper(ThumbnailUrlBuildImpl()),TrampolineRxSchedulerProvider())
        viewModel.load(character.id)
        Assert.assertTrue(viewModel.state.value is ViewModelState.Error)
    }

}