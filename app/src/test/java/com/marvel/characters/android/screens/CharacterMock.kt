package com.marvel.characters.android.screens

import com.marvel.characters.entities.Character
import com.marvel.characters.entities.Thumbnail
import com.marvel.characters.entities.Url
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Rafael Decker on 2020-01-18.
 */

object CharacterMock {

    fun getItems(number: Int): List<Character> {
        val list = ArrayList<Character>()
        for (index in 0 until number) {
            list.add(
                Character(
                    id = index.toLong(),
                    name = "$index",
                    description = "Lorem$index",
                    thumbnail = Thumbnail("$index", "png"),
                    modified = Date(),
                    comics = listOf("comic1", "comic2"),
                    series = listOf("serie1", "serie2"),
                    stories = listOf("stories1", "stories2", "stories3"),
                    urls = listOf(Url("type", "http://www.google.com"))
                )
            )
        }
        return list
    }

}