package com.marvel.characters.android.screens.home

import com.marvel.characters.domain.ThumbnailUrlBuilder
import com.marvel.characters.entities.Character
import com.marvel.characters.entities.ImageSize
import com.marvel.characters.entities.Thumbnail
import com.marvel.characters.util.Mapper
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class HomeModelMapperTest {

    @Mock
    lateinit var thumbnailUrlBuilder: ThumbnailUrlBuilder

    lateinit var mapper: Mapper<Character, HomeModel>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        mapper = HomeModelMapper(thumbnailUrlBuilder)
    }

    @Test
    fun `test given character with description mapper is mapping accordingly`() {
        val character = getCharacter(description = "description")
        val size = ImageSize.PORTRAIT_XLARGE
        val thumbnailString = "path/portrait_xlarge.png"
        `when`(thumbnailUrlBuilder.build(character.thumbnail, size)).thenReturn(thumbnailString)
        val result = mapper.map(character)
        assertEquals(character.id, result.id)
        assertEquals(character.name, result.name)
        assertEquals(character.description, result.description)
        assertEquals(thumbnailString, result.thumbnail)
        assertTrue(result.isInfoAvailable)
    }

    @Test
    fun `test given character with no description mapper is mapping accordingly`() {
        val character = getCharacter(description = "")
        val size = ImageSize.PORTRAIT_XLARGE
        val thumbnailString = "path/portrait_xlarge.png"
        `when`(thumbnailUrlBuilder.build(character.thumbnail, size)).thenReturn(thumbnailString)
        val result = mapper.map(character)
        assertEquals(character.id, result.id)
        assertEquals(character.name, result.name)
        assertEquals(character.description, result.description)
        assertEquals(thumbnailString, result.thumbnail)
        assertFalse(result.isInfoAvailable)
    }

    @Test(expected = NotImplementedError::class)
    fun `test map reverse is not implemented`() {
        val character = getCharacter(description = "description")
        val size = ImageSize.PORTRAIT_XLARGE
        val thumbnailString = "path/portrait_xlarge.png"
        `when`(thumbnailUrlBuilder.build(character.thumbnail, size)).thenReturn(thumbnailString)
        val mapped = mapper.map(character)
        mapper.mapReverse(mapped)
    }

    private fun getCharacter(description: String) =
        Character(
            id = 10,
            name = "name",
            description = description,
            thumbnail = Thumbnail("path", "png"),
            modified = Date(),
            comics = emptyList(),
            series = emptyList(),
            stories = emptyList(),
            urls = emptyList()
        )

}