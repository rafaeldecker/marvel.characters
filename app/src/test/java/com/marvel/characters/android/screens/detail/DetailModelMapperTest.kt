package com.marvel.characters.android.screens.detail

import com.marvel.characters.domain.ThumbnailUrlBuilder
import com.marvel.characters.entities.Character
import com.marvel.characters.entities.ImageSize
import com.marvel.characters.entities.Thumbnail
import com.marvel.characters.entities.Url
import com.marvel.characters.util.Mapper
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class DetailModelMapperTest {

    @Mock
    lateinit var thumbnailUrlBuilder: ThumbnailUrlBuilder

    lateinit var mapper: Mapper<Character, DetailModel>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val size = ImageSize.PORTRAIT_XLARGE
        Mockito.`when`(thumbnailUrlBuilder.build(THUMBNAIL, size)).thenReturn(THUMBNAIL_RESULT)
        mapper = DetailModelMapper(thumbnailUrlBuilder)
    }

    @Test(expected = NotImplementedError::class)
    fun `test map reverse is not implemented`() {
        mapper.mapReverse(mapper.map(getCharacter()))
    }

    @Test
    fun `test given character is mapping character section accordingly`() {
        val character = getCharacter()
        val result = mapper.map(character)
        assertTrue(result.sections.size == 1)
        assertTrue(result.sections[0] is DetailSection.Character)
        val section = result.sections[0] as DetailSection.Character
        assertEquals(FORMATTED_MODIFIED, section.modified)
        assertEquals(THUMBNAIL_RESULT, section.thumbnail)
        assertEquals(character.description, section.description)
        assertEquals(character.name, section.name)
        assertEquals(character.id, section.id)
    }

    @Test
    fun `test given character with comic is mapping sections accordingly`() {
        val character = getCharacter(comics = listOf("1", "2"))
        val result = mapper.map(character)
        assertTrue(result.sections.size == 4)
        assertTrue(result.sections[1] is DetailSection.Comics)
        assertTrue(result.sections[2] is DetailSection.SimpleItem)
        assertTrue(result.sections[3] is DetailSection.SimpleItem)
        assertEquals("1", (result.sections[2] as DetailSection.SimpleItem).data)
        assertEquals("2", (result.sections[3] as DetailSection.SimpleItem).data)
    }

    @Test
    fun `test given character with series is mapping sections accordingly`() {
        val character = getCharacter(series = listOf("1", "2"))
        val result = mapper.map(character)
        assertTrue(result.sections.size == 4)
        assertTrue(result.sections[1] is DetailSection.Series)
        assertTrue(result.sections[2] is DetailSection.SimpleItem)
        assertTrue(result.sections[3] is DetailSection.SimpleItem)
        assertEquals("1", (result.sections[2] as DetailSection.SimpleItem).data)
        assertEquals("2", (result.sections[3] as DetailSection.SimpleItem).data)
    }

    @Test
    fun `test given character with stories is mapping sections accordingly`() {
        val character = getCharacter(stories = listOf("1", "2"))
        val result = mapper.map(character)
        assertTrue(result.sections.size == 4)
        assertTrue(result.sections[1] is DetailSection.Stories)
        assertTrue(result.sections[2] is DetailSection.SimpleItem)
        assertTrue(result.sections[3] is DetailSection.SimpleItem)
        assertEquals("1", (result.sections[2] as DetailSection.SimpleItem).data)
        assertEquals("2", (result.sections[3] as DetailSection.SimpleItem).data)
    }

    @Test
    fun `test given character with urls is mapping sections accordingly`() {
        val character = getCharacter(urls = listOf(
            Url("abc", "1"),
            Url("def", "2"))
        )
        val result = mapper.map(character)
        assertTrue(result.sections.size == 4)
        assertTrue(result.sections[1] is DetailSection.Urls)
        assertTrue(result.sections[2] is DetailSection.UrlDetail)
        assertTrue(result.sections[3] is DetailSection.UrlDetail)
        assertEquals("abc", (result.sections[2] as DetailSection.UrlDetail).type)
        assertEquals("1", (result.sections[2] as DetailSection.UrlDetail).url)
        assertEquals("def", (result.sections[3] as DetailSection.UrlDetail).type)
        assertEquals("2", (result.sections[3] as DetailSection.UrlDetail).url)
    }

    private fun getCharacter(
        comics: List<String> = emptyList(),
        series: List<String> = emptyList(),
        stories: List<String> = emptyList(),
        urls: List<Url> = emptyList()
    ) =
        Character(
            id = 983247,
            name = "name",
            description = "description",
            thumbnail = THUMBNAIL,
            modified = getModified(),
            comics = comics,
            series = series,
            stories = stories,
            urls = urls
        )

    private fun getModified(): Date {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, 2020)
        calendar.set(Calendar.MONTH, 11)
        calendar.set(Calendar.DAY_OF_MONTH, 16)
        return calendar.time
    }

    companion object {
        private val THUMBNAIL = Thumbnail("path", "png")
        private const val FORMATTED_MODIFIED = "12/16/2020"
        private const val THUMBNAIL_RESULT = "path/portrait_xlarge.png"
    }

}