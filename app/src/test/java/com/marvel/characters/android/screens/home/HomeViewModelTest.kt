package com.marvel.characters.android.screens.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.marvel.characters.android.screens.CharacterMock
import com.marvel.characters.android.screens.base.ViewModelState
import com.marvel.characters.domain.GetCharactersUseCase
import com.marvel.characters.entities.Character
import com.marvel.characters.infra.schedulers.RxSchedulerProvider
import com.marvel.characters.infra.schedulers.TrampolineRxSchedulerProvider
import com.marvel.characters.util.Mapper
import io.reactivex.Observable
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.internal.verification.Times

/**
 * Created by Rafael Decker on 2020-01-19.
 */

class HomeViewModelTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var useCase: GetCharactersUseCase

    @Mock
    lateinit var mapper: Mapper<Character, HomeModel>


    private lateinit var viewModel: StateHomeViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = StateHomeViewModel(useCase, mapper, TrampolineRxSchedulerProvider())
    }

    @Test
    fun `test given use case response is calling model mapper`() {
        val list = CharacterMock.getItems(4)
        `when`(useCase.fetch(0, 20)).thenReturn(Observable.just(list))
        viewModel.start()
        verify(mapper, Times(1)).mapList(list)
        assertTrue(viewModel.state.value is ViewModelState.Data<*>)
    }

    @Test
    fun `test given use case error view model state is error`() {
        `when`(useCase.fetch(0, 20)).thenReturn(Observable.error(Throwable("any")))
        viewModel.start()
        assertTrue(viewModel.state.value is ViewModelState.Error)
    }

    @Test
    fun `test given use case response is just one page load more is not loading more data`() {
        val list = CharacterMock.getItems(4)
        `when`(useCase.fetch(0, 20)).thenReturn(Observable.just(list))
        viewModel.start()
        viewModel.loadMore()
        verify(mapper, Times(1)).mapList(list)
        assertTrue(viewModel.state.value is ViewModelState.Data<*>)
    }

    @Test
    fun `test given use case response is more than one page load more is loading more`() {
        val list = CharacterMock.getItems(20)
        `when`(useCase.fetch(0, 20)).thenReturn(Observable.just(list))
        `when`(useCase.fetch(20, 20)).thenReturn(Observable.just(list))
        viewModel.start()
        viewModel.loadMore()
        verify(mapper, Times(2)).mapList(list)
        assertTrue(viewModel.state.value is ViewModelState.Data<*>)
    }

    @Test
    fun `test given loading state load more can not load items`() {
        val list = CharacterMock.getItems(20)
        `when`(useCase.fetch(0, 20)).thenReturn(Observable.just(list))
        viewModel = StateHomeViewModel(useCase, mapper, TrampolineRxSchedulerProvider(), ViewModelState.Loading)
        viewModel.loadMore()
        verify(mapper, never()).mapList(list)
        assertTrue(viewModel.state.value is ViewModelState.Loading)
    }

    inner class StateHomeViewModel(
        useCase: GetCharactersUseCase,
        mapper: Mapper<Character, HomeModel>,
        rxSchedulerProvider: RxSchedulerProvider,
        private val st: ViewModelState? = null
    ) : HomeViewModel(useCase, mapper, rxSchedulerProvider) {

        init {
            st?.let {
                updateState(it)
            }
        }

    }

}