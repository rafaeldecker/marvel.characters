package com.marvel.characters.infra.api

import com.marvel.characters.infra.api.authorization.ApiAuthorizationProvider
import com.marvel.characters.infra.api.authorization.ApiAuthorizationProviderImpl
import com.marvel.characters.infra.api.keys.ApiKeys
import com.marvel.characters.infra.api.keys.ApiKeysProvider
import com.marvel.characters.infra.timestamp.TimestampProvider
import com.marvel.characters.util.extensions.toMd5
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ApiAuthorizationProviderTest {

    @Mock
    lateinit var apiKeysProvider: ApiKeysProvider

    @Mock
    lateinit var timestampProvider: TimestampProvider

    private val keys = ApiKeys(
        public = PUBLIC_KEY,
        private = PRIVATE_KEY
    )

    lateinit var apiAuthorizationProvider: ApiAuthorizationProvider

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        `when`(timestampProvider.time()).thenReturn(TIMESTAMP)
        `when`(apiKeysProvider.getKeys()).thenReturn(keys)
        apiAuthorizationProvider = ApiAuthorizationProviderImpl(apiKeysProvider, timestampProvider)
    }

    @Test
    fun `test given timestamp and api keys is returning right api authorization`() {
        val auth = apiAuthorizationProvider.generate()
        assertEquals(PUBLIC_KEY, auth.apiKey)
        assertEquals(TIMESTAMP, auth.timestamp)
        assertEquals(HASH.toMd5(), auth.hash)

    }

    companion object {
        private const val PUBLIC_KEY = "public"
        private const val PRIVATE_KEY = "private"
        private const val TIMESTAMP = 1579376437L
        private const val HASH = "$TIMESTAMP$PRIVATE_KEY$PUBLIC_KEY"
    }


}