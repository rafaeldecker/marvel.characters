package com.marvel.characters.infra.api

import com.marvel.characters.BuildConfig
import com.marvel.characters.infra.api.keys.ApiKeysProviderImpl
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ApiKeysProviderTest {

    @Test
    fun `test api keys provider is returning right keys`() {
        val apiKeysProvider = ApiKeysProviderImpl()
        val keys = apiKeysProvider.getKeys()
        assertEquals(BuildConfig.API_PUBLIC_KEY, keys.public)
        assertEquals(BuildConfig.API_PRIVATE_KEY, keys.private)
    }

}