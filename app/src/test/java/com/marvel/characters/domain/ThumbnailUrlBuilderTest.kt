package com.marvel.characters.domain

import com.marvel.characters.entities.ImageSize
import com.marvel.characters.entities.Thumbnail
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class ThumbnailUrlBuilderTest {

    @Test
    fun `test thumbnail url builder is building right url`() {
        val thumb = Thumbnail("path", "jpg")
        val builder = ThumbnailUrlBuildImpl()
        val size = ImageSize.PORTRAIT_INCREDIBLE
        assertEquals("${thumb.path}/${size.rawValue}.${thumb.extension}", builder.build(thumb, size))
    }

}