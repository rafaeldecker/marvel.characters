package com.marvel.characters.domain

import com.marvel.characters.entities.Character
import com.marvel.characters.infra.api.CharactersApi
import com.marvel.characters.infra.api.authorization.ApiAuthorization
import com.marvel.characters.infra.api.authorization.ApiAuthorizationProvider
import com.marvel.characters.infra.api.models.ApiResponse
import com.marvel.characters.infra.api.models.CharacterResponse
import com.marvel.characters.infra.api.models.PaginationResponse
import com.marvel.characters.util.Mapper
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.internal.verification.Times

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class GetCharactersUseCaseTest {

    @Mock
    lateinit var apiAuthorizationProvider: ApiAuthorizationProvider

    @Mock
    lateinit var api: CharactersApi

    @Mock
    lateinit var mapper: Mapper<CharacterResponse, Character>

    private lateinit var useCase: GetCharactersUseCase

    private lateinit var pagination: PaginationResponse<CharacterResponse>
    private lateinit var apiResponse: ApiResponse<CharacterResponse>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.reset()
        RxAndroidPlugins.setInitMainThreadSchedulerHandler {
            Schedulers.trampoline()
        }
        pagination = PaginationResponse(
            offset = 1,
            limit = 1,
            total = 1,
            count = 1,
            results = listOf(CharacterResponseMock.data)
        )
        apiResponse = ApiResponse(pagination)
        `when`(apiAuthorizationProvider.generate()).thenReturn(API_AUTHORIZATION)
        useCase = GetCharactersUseCaseImpl(apiAuthorizationProvider, api, mapper)
    }

    @Test
    fun `test use case fetch is calling api and mapper accordingly`() {
        `when`(
            api.get(
                timeStamp = API_AUTHORIZATION.timestamp,
                apiKey = API_AUTHORIZATION.apiKey,
                hash = API_AUTHORIZATION.hash,
                offset = 0,
                limit = 20
            )
        ).thenReturn(Observable.just(apiResponse))
        useCase.fetch(0, 20).test()
        verify(api, Times(1)).get(
            timeStamp = API_AUTHORIZATION.timestamp,
            apiKey = API_AUTHORIZATION.apiKey,
            hash = API_AUTHORIZATION.hash,
            offset = 0,
            limit = 20
        )
        verify(mapper, Times(1)).mapList(apiResponse.data.results)
    }

    @Test
    fun `test use case fetch detail is calling api and mapper accordingly`() {
        val characterId = 1234L
        `when`(
            api.getCharacter(
                characterId = characterId,
                timeStamp = API_AUTHORIZATION.timestamp,
                apiKey = API_AUTHORIZATION.apiKey,
                hash = API_AUTHORIZATION.hash
            )
        ).thenReturn(Single.just(apiResponse))
        useCase.fetchDetail(characterId).test()
        verify(api, Times(1)).getCharacter(
            characterId = characterId,
            timeStamp = API_AUTHORIZATION.timestamp,
            apiKey = API_AUTHORIZATION.apiKey,
            hash = API_AUTHORIZATION.hash
        )
        verify(mapper, Times(1)).mapList(apiResponse.data.results)
    }

    @Test
    fun `test use case fetch detail is throwing exception when api returns empty list`() {
        val characterId = 1234L
        pagination = PaginationResponse(
            offset = 1,
            limit = 1,
            total = 1,
            count = 1,
            results = emptyList()
        )
        apiResponse = ApiResponse(pagination)
        `when`(
            api.getCharacter(
                characterId = characterId,
                timeStamp = API_AUTHORIZATION.timestamp,
                apiKey = API_AUTHORIZATION.apiKey,
                hash = API_AUTHORIZATION.hash
            )
        ).thenReturn(Single.just(apiResponse))
        `when`(mapper.mapList(apiResponse.data.results)).thenReturn(emptyList())
        val testObserver: TestObserver<Character> = useCase.fetchDetail(characterId).test()
        testObserver.assertFailure(Throwable::class.java)
    }

    companion object {
        private val API_AUTHORIZATION = ApiAuthorization(
            timestamp = 2318478374L,
            apiKey = "key",
            hash = "hash"
        )
    }

}