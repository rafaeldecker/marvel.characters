package com.marvel.characters.domain

import com.marvel.characters.infra.api.models.*
import java.util.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

object CharacterResponseMock {

    val data =
        CharacterResponse(
            id = 10L,
            name = "Fake name",
            description = "Lorem ipsum",
            modified = Date(),
            thumbnail = ThumbnailResponse("abc", "jpg"),
            resourceURI = "test",
            comics = CollectionResponse(
                collectionURI = "comics",
                items = listOf(
                    ItemResponse("comics1", "comicsRes1"),
                    ItemResponse("comics2", "comicsRes2")
                )
            ),
            series = CollectionResponse(
                collectionURI = "series",
                items = listOf(
                    ItemResponse("series1", "seriesRes1"),
                    ItemResponse("series2", "seriesRes2"),
                    ItemResponse("series3", "seriesRes3")
                )
            ),
            stories = CollectionResponse(
                collectionURI = "stories",
                items = listOf(
                    TypedItem("stories1", "storiesRes1", "type1"),
                    TypedItem("stories2", "storiesRes2", "type2"),
                    TypedItem("stories3", "storiesRes3", "type3"),
                    TypedItem("stories4", "storiesRes4", "type4")
                )
            ),
            urls = listOf(
                UrlResponse(type = "t1", url = "http://developer.google.com"),
                UrlResponse(type = "t2", url = "http://developer.android.com")
            )
        )

}