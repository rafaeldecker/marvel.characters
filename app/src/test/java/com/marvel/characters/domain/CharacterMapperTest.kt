package com.marvel.characters.domain

import com.marvel.characters.entities.Character
import com.marvel.characters.infra.api.models.*
import com.marvel.characters.util.Mapper
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.util.*

/**
 * Created by Rafael Decker on 2020-01-18.
 */

class CharacterMapperTest {

    private lateinit var mapper: Mapper<CharacterResponse, Character>

    @Before
    fun setup() {
        mapper = CharacterMapper()
    }

    @Test
    fun `test character mapper is mapping api response on a right way`() {
        val response = CharacterResponseMock.data
        val mapped = mapper.map(response)
        assertEquals(response.id, mapped.id)
        assertEquals(response.name, mapped.name)
        assertEquals(response.description, mapped.description)
        assertEquals(response.thumbnail.path, mapped.thumbnail.path)
        assertEquals(response.thumbnail.extension, mapped.thumbnail.extension)
        assertEquals(response.modified, mapped.modified)
        for ((index, value) in response.comics.items.withIndex()) {
            assertEquals(value.name, mapped.comics[index])
        }
        for ((index, value) in response.series.items.withIndex()) {
            assertEquals(value.name, mapped.series[index])
        }
        for ((index, value) in response.stories.items.withIndex()) {
            assertEquals(value.name, mapped.stories[index])
        }
        for ((index, value) in response.urls.withIndex()) {
            assertEquals(value.type, mapped.urls[index].type)
            assertEquals(value.url, mapped.urls[index].url)
        }
    }

    @Test(expected = NotImplementedError::class)
    fun `test map reverse is not implemented`() {
        val response = CharacterResponseMock.data
        val mapped = mapper.map(response)
        mapper.mapReverse(mapped)
    }


}